FROM couchbase/server:enterprise-4.6.0

COPY configure-node.sh /opt/couchbase

CMD ["/opt/couchbase/configure-node.sh"]

