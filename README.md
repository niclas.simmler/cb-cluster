# Pull Yaml to Gcloud

curl https://gitlab.com/niclas.simmler/cb-cluster/raw/master/couchbase-deployment.yml -o couchbase-deployment.yml


# Build instance
~~~~
gcloud compute instances create thebrain-couchbase \
    --image-family cos-stable \
    --image-project cos-cloud \
    --metadata-from-file google-container-manifest=couchbase-deployment.yml \
    --zone europe-west1-c \
    --machine-type f1-micro
~~~~
